﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using HarmonyLib;
using Helpers;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.CharacterDevelopment.Managers;
using TaleWorlds.CampaignSystem.SandBox.CampaignBehaviors;
using TaleWorlds.CampaignSystem.SandBox.GameComponents;
using TaleWorlds.CampaignSystem.SandBox.GameComponents.Map;
using TaleWorlds.Core;
using TaleWorlds.Library;
using TaleWorlds.Localization;
using TaleWorlds.MountAndBlade;
// ReSharper disable RedundantAssignment
// ReSharper disable InconsistentNaming
// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedParameter.Local
// ReSharper disable RedundantArgumentDefaultValue

namespace RationalLeveling_RBM
{
	/// <summary>
	/// Based on the mod "Leveling Reblance"
	/// </summary>
	public class RationalLevelingSubModule : MBSubModuleBase
	{
		internal const int MaxLevel = 75;

		protected override void OnBeforeInitialModuleScreenSetAsRoot()
		{
			base.OnBeforeInitialModuleScreenSetAsRoot();
			InformationManager.DisplayMessage(new InformationMessage("RationalLeveling_RBM 1.0.0"));
		}
		protected override void OnSubModuleLoad()
		{
			LogXpDifferences();

			base.OnSubModuleLoad();
			new Harmony("RationalLeveling_RBM").PatchAll(typeof(RationalLevelingSubModule).Assembly);
			new Harmony("RationalLeveling_RBM").PatchAll();
		}

		/// <summary>
		/// Print to the harmony log on the desktop what the new vs old XP required for the different levels
		/// </summary>
		public static void LogXpDifferences()
		{
			try
			{
				DefaultCharacterDevelopmentModel dcdm = new DefaultCharacterDevelopmentModel();
				int[] xpRequiredForSkillLevel = new int[1024];
				int[] skillsRequiredForLevel = new int[1024];

				PatchXpRequiredForSkillLevel.Prefix(ref xpRequiredForSkillLevel);
				PatchSkillRequiredForLevel.Prefix(ref skillsRequiredForLevel);

				FileLog.Log("");
				FileLog.Log("SkillLevel, RationalXpRequiredForSkillPoint, BaseXpRequiredForSkillPoint, PctDelta");
				for (int i = 1; i < 400; i++)
				{
					if (i > 10 && i % 10 != 0)
					{
						continue;
					}

					int newXp = xpRequiredForSkillLevel[i - 1];
					int oldXp = Math.Max(dcdm.GetXpRequiredForSkillLevel(i), 1);
					FileLog.Log($"{i}, {newXp}, {oldXp}, {newXp / (double)oldXp}");
				}

				FileLog.Log("");
				FileLog.Log("CharacterLevel, RationalXPRequiredForLevel, BaseXPRequiredForLevel, PctDelta");
				for (int i = 0; i < 75; i++)
				{
					if (i > 5 && i % 5 != 0)
					{
						continue;
					}

					int newXp = skillsRequiredForLevel[i];
					int oldXp = Math.Max(dcdm.SkillsRequiredForLevel(i), 1);
					FileLog.Log($"{i}, {newXp}, {oldXp}, {newXp / (double)oldXp}");
				}
			}
			catch (Exception e)
			{
				FileLog.Log(e.ToString());
			}
			FileLog.Log("");
		}
	}


	[HarmonyPatch(typeof(DefaultCharacterDevelopmentModel), "InitializeXpRequiredForSkillLevel")]
	internal class PatchXpRequiredForSkillLevel
	{
		public static bool Prefix(ref int[] ____xpRequiredForSkillLevel)
		{
			____xpRequiredForSkillLevel = new int[1024];

			int num = 0;
			____xpRequiredForSkillLevel[0] = num;
			for (int index = 1; index < ____xpRequiredForSkillLevel.Length; ++index)
			{
				if (index < 50)
				{
					num += 20;
				}
				else if (index < 100)
				{
					num += 30;
				}
				else if (index < 200)
				{
					num += 50;
				}
				else if (index <= 300)
				{
					num += 110;
				}
				else
				{
					num += index * 2;
				}
				____xpRequiredForSkillLevel[index] = ____xpRequiredForSkillLevel[index - 1] + num;
			}

			FileLog.Log("PatchXpRequiredForSkillLevel complete");
			return false;
		}
	}

	[HarmonyPatch(typeof(DefaultCharacterDevelopmentModel), "InitializeSkillsRequiredForLevel")]
	internal class PatchSkillRequiredForLevel
	{
		public static bool Prefix(ref int[] ____skillsRequiredForLevel)
		{
			int num = 2000;
			____skillsRequiredForLevel = new int[1000];
			____skillsRequiredForLevel[0] = 0;
			____skillsRequiredForLevel[1] = 1;
			for (int i = 2; i < ____skillsRequiredForLevel.Length; ++i)
			{
				____skillsRequiredForLevel[i] = ____skillsRequiredForLevel[i - 1] + num;

				if (i < 10)
				{
					num += 1500;
				}
				else if (i < 20)
				{
					num += 2200;
				}
				else if (i < 30)
				{
					num += 3500;
				}
				else if (i < 40)
				{
					num += 5500;
				}
				else
				{
					num += 8000;
				}
			}

			FileLog.Log("PatchSkillRequiredForLevel complete");
			return false;
		}
	}

	[HarmonyPatch(typeof(DefaultCharacterDevelopmentModel))]
	[HarmonyPatch("SkillsRequiredForLevel")]
	public static class PatchSkillsRequiredForLevel
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			foreach (CodeInstruction ci in codes)
			{
				if (ci.opcode == OpCodes.Ldc_I4_S)
				{
					ci.operand = RationalLevelingSubModule.MaxLevel; // vanilla = 62
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(DefaultCharacterDevelopmentModel), "get_LevelsPerAttributePoint")]
	public static class PatchLevelsPerAttributePoint
	{
		private static void Postfix(ref int __result)
		{
			__result = 3; // vanilla = 4
		}
	}

	[HarmonyPatch(typeof(DefaultCharacterDevelopmentModel), "CalculateLearningRate", new[] { typeof(int), typeof(int), typeof(int), typeof(int), typeof(TextObject), typeof(bool) })]
	internal class PatchCalculateLearningRate
	{
		private static TextObject _attributeText = new TextObject("{=AT6v10NK}Attribute");
		private static readonly TextObject _skillFocusText = new TextObject("{=MRktqZwu}Skill Focus");
		private static readonly TextObject _overLimitText = new TextObject("{=bcA7ZuyO}Learning Limit Exceeded");
		public static bool Prefix(ref ExplainedNumber __result, ref DefaultCharacterDevelopmentModel __instance, int attributeValue, int focusValue, int skillValue, int characterLevel, TextObject attributeName, bool includeDescriptions = false)
		{
			ExplainedNumber explainedNumber = new ExplainedNumber(1.0f, true, null); // vanilla = "ExplainedNumber explainedNumber = new ExplainedNumber(1.25f, includeDescriptions, (TextObject) null);"
			explainedNumber.AddFactor((float)(attributeValue * .5), attributeName); // vanilla = 0.4f
			explainedNumber.AddFactor(focusValue, _skillFocusText);
			int learningLimit = MathF.Round(__instance.CalculateLearningLimit(attributeValue, focusValue, null, false).ResultNumber);
			if (skillValue >= learningLimit)
			{
				float overAmount = skillValue - learningLimit;
				explainedNumber.AddFactor((float) (-1.0 + -0.2 * overAmount), _overLimitText);
			}
			explainedNumber.LimitMin(0.05f);
			__result = explainedNumber;
			return false;
		}
	}

	[HarmonyPatch(typeof(DefaultCharacterDevelopmentModel), "CalculateLearningLimit", new[] { typeof(int), typeof(int), typeof(TextObject), typeof(bool) })]
	internal class PatchCalculateLearningLimit
	{
		private static readonly TextObject _skillFocusText = new TextObject("{=MRktqZwu}Skill Focus", null);
		public static bool Prefix(ref ExplainedNumber __result, ref DefaultCharacterDevelopmentModel __instance, int attributeValue, int focusValue, TextObject attributeName, bool includeDescriptions = false)
		{
			ExplainedNumber explainedNumber = new ExplainedNumber(50f, includeDescriptions, null); // vanilla = 0f
																											   // vanilla "explainedNumber.Add((float)((attributeValue - 1) * 10), attributeName, (TextObject)null);"
			explainedNumber.Add(focusValue * 50, _skillFocusText, null); // vanilla = 30
			explainedNumber.LimitMin(0.0f);
			__result = explainedNumber;
			return false;
		}
	}

	[HarmonyPatch(typeof(HeroDeveloper), "<SetInitialLevelFromSkills>b__56_0")]
	internal class Patchb__56_0
	{
		public static bool Prefix(ref HeroDeveloper __instance, ref float __result, SkillObject s)
		{
			float sl = __instance.Hero.GetSkillValue(s);
			__result = 0.50f * 15 * sl * ((sl + 1) / 2);
			return false;
		}
	}

	[HarmonyPatch(typeof(DefaultSmithingModel))]
	[HarmonyPatch("GetSkillXpForRefining")]
	public static class PatchGetSkillXpForRefining
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			foreach (CodeInstruction ci in codes)
			{
				if (ci.opcode == OpCodes.Ldc_R4)
				{
					ci.operand = 0.1f; // vanilla = 0.3f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	// Standard version

	[HarmonyPatch(typeof(DefaultSmithingModel), "GetSkillXpForSmelting")]
	internal class PatchGetSkillXpForSmelting
	{
		public static bool Prefix(ref int __result, ItemObject item)
		{
			__result = MathF.Round(25.2f * MathF.Pow(item.Value, 0.165f)); // vanilla = "MathF.Round(0.02f * (float) item.Value);"
			return false;
		}
	}

	[HarmonyPatch(typeof(DefaultSmithingModel), "GetSkillXpForSmithingInFreeBuildMode")]
	internal class PatchGetSkillXpForSmithingInFreeBuildMode
	{
		public static bool Prefix(ref int __result, ItemObject item)
		{
			__result = MathF.Round(63f * MathF.Pow(item.Value, 0.165f)); // vanilla = "MathF.Round(0.1f * (float) item.Value);"
			return false;
		}
	}

		[HarmonyPatch(typeof(DefaultSmithingModel), "GetSkillXpForSmithingInCraftingOrderMode")]
	internal class PatchGetSkillXpForSmithingInCraftingOrderMode
	{
		public static bool Prefix(ref int __result, ItemObject item)
		{
			__result = MathF.Round(252f * MathF.Pow(item.Value, 0.165f)); // vanilla = "MathF.Round(0.1f * (float) item.Value);"
			return false;
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager), "OnHighMorale")]
	internal class PatchOnHighMorale
	{
		private static void OnLeaderSkillExercised(PartyBase party, SkillObject skill, float skillXp)
		{
			party.LeaderHero?.HeroDeveloper.AddSkillXp(skill, skillXp, true, true);
		}
		public static bool Prefix(MobileParty party)
		{
			OnLeaderSkillExercised(party.Party, DefaultSkills.Leadership, MathF.Round(0.15f * MathF.Pow(party.MemberRoster.TotalManCount, 0.7f) * MathF.Pow((float)Math.Max(0, party.Morale - 60.0), 0.5f))); // vanilla = "SkillLevelingManager.OnLeaderSkillExercised(party.Party, DefaultSkills.Leadership, (float) MathF.Round((float) (0.01 * (double) party.MemberRoster.TotalManCount * ((double) party.Morale - 70.0))));"
			return false;
		}
	}

	[HarmonyPatch(typeof(MobilePartyTrainingBehavior))]
	[HarmonyPatch("DailyTickParty")]
	public static class PatchDailyTickParty
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			foreach (CodeInstruction ci in codes)
			{
				if (ci.opcode == OpCodes.Ldc_R4)
				{
					ci.operand = 60f; // vanilla = 75f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(BanditsCampaignBehavior))]
	[HarmonyPatch("DailyTick")]
	public static class PatchDailyTick
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			foreach (CodeInstruction ci in codes)
			{
				if (ci.opcode == OpCodes.Ldc_R4 && (float)ci.operand == 70f)
				{
					ci.operand = 60f; // vanilla = 70f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager))]
	[HarmonyPatch("OnLeadingArmy")]
	public static class PatchOnLeadingArmy
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			foreach (CodeInstruction ci in codes)
			{
				if (ci.opcode == OpCodes.Ldc_R4)
				{
					ci.operand = 0.0004f; // vanilla = 0.0004f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(DefaultMapTrackModel), "GetSkillFromTrackDetected")]
	internal class PatchGetSkillFromTrackDetected
	{
		private static bool Prefix(ref float __result, Track track, float detectionDifficulty)
		{
			/*
			float num = (float)(0.03 * (1.0 + (double)track.CreationTime.ElapsedHoursUntilNow) * (1.0 + 0.02 * (double)Math.Max(0.0f, 100f - (float)track.NumberOfAllMembers))); // vanilla = "float num = (float) (0.2 * (1.0 + (double) track.CreationTime.ElapsedHoursUntilNow) * (1.0 + 0.02 * (double) Math.Max(0.0f, 100f - (float) track.NumberOfAllMembers)));"
			if (track.IsEnemy)
				num *= track.PartyType == Track.PartyTypeEnum.Lord ? 10f : (track.PartyType == Track.PartyTypeEnum.Bandit ? 4f : (track.PartyType == Track.PartyTypeEnum.Caravan ? 3f : 2f));
			__result = num;
			*/
			__result = 5f;
			return false;
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager))]
	[HarmonyPatch("OnHideoutSpotted")]
	public static class PatchOnHideoutSpotted
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			foreach (CodeInstruction ci in codes)
			{
				if (ci.opcode == OpCodes.Ldc_R4)
				{
					ci.operand = 600f; // vanilla = 50f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager), "OnPartyQuartermasterSkillExercised")]
	internal class PatchOnPartyQuartermasterSkillExercised
	{
		private static bool Prefix(MobileParty party, SkillObject skill, float skillXp)
		{
			party.EffectiveQuartermaster?.AddSkillXp(skill, MathF.Pow(skillXp, 0.7f)); // vanilla = "party.EffectiveQuartermaster?.AddSkillXp(skill, skillXp);"
			return false;
		}
	}

	[HarmonyPatch(typeof(DefaultPartyHealingModel), "GetSkillXpFromHealingTroop")]
	internal class PatchGetSkillXpFromHealingTroop
	{
		private static void Postfix(ref int __result)
		{
			__result = 100; // vanilla = 10
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager))]
	[HarmonyPatch("OnHeroHealedWhileWaiting")]
	public static class PatchOnHeroHealedWhileWaiting
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			foreach (CodeInstruction ci in codes)
			{
				if (ci.opcode == OpCodes.Ldc_R4)
				{
					ci.operand = 0.10f; // vanilla = 0.01f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager), "OnCombatHit")]
	internal class PatchOnCombatHit
	{
		private static bool Prefix(
		CharacterObject affectorCharacter,
		CharacterObject affectedCharacter,
		CharacterObject captain,
		Hero commander,
		float speedBonusFromMovement,
		float shotDifficulty,
		WeaponComponentData affectorWeapon,
		float hitPointRatio,
		CombatXpModel.MissionTypeEnum missionType,
		bool isAffectorMounted,
		bool isTeamKill,
		bool isAffectorUnderCommand,
		float damageAmount,
		bool isFatal)
		{
			if (isTeamKill)
				return false;
			if (affectorCharacter.IsHero)
			{
				Hero heroObject = affectorCharacter.HeroObject;
				Campaign.Current.Models.CombatXpModel.GetXpFromHit(heroObject.CharacterObject, captain, affectedCharacter, heroObject.PartyBelongedTo?.Party, (int)damageAmount, isFatal, missionType, out int xpAmount);
				float xpAmountAdjusted = xpAmount;
				if (affectorWeapon != null)
				{
					SkillObject skillForWeapon = Campaign.Current.Models.CombatXpModel.GetSkillForWeapon(affectorWeapon);
					float num = skillForWeapon == DefaultSkills.Bow ? 0.75f : skillForWeapon == DefaultSkills.Throwing ? 1.5f : 1f; // "float num = skillForWeapon == DefaultSkills.Bow ? 0.5f : 1f;"
					if (shotDifficulty > 0.0)
					{
						xpAmountAdjusted += MathF.Floor(xpAmountAdjusted * num * Campaign.Current.Models.CombatXpModel.GetXpMultiplierFromShotDifficulty(shotDifficulty));
						xpAmountAdjusted *= 0.66f; // New code
					}
					if (!affectorCharacter.IsPlayerCharacter)
					{
						if (skillForWeapon == DefaultSkills.Bow || skillForWeapon == DefaultSkills.Crossbow || skillForWeapon == DefaultSkills.Throwing)
						{
							xpAmountAdjusted *= 5;
						}
						else
						{
							xpAmountAdjusted *= 10;
						}
					}
					xpAmountAdjusted *= 0.4f; // Multiplier for easy tweaking
					heroObject.AddSkillXp(skillForWeapon, MBRandom.RoundRandomized(xpAmountAdjusted));
				}
				else
                {
					if (!affectorCharacter.IsPlayerCharacter)
                    {
						xpAmountAdjusted *= 10;
					}
					heroObject.AddSkillXp(DefaultSkills.Athletics, MBRandom.RoundRandomized(xpAmountAdjusted));
				}
				if (isAffectorMounted)
				{
					float num1 = 0.5f; // vanilla = 0.1f
					if (speedBonusFromMovement > 0.0)
					{
						num1 += speedBonusFromMovement / 4; // vanilla = "num1 *= 1f + speedBonusFromMovement;"
					}
					/* deactivated vanilla code
					else if ((double) shotDifficulty - 1.0 > 0.0)
					{
						int num2 = MathF.Round(shotDifficulty - 1f);
						if (num2 > 0)
						num1 += (float) num2;
					}
					*/
					if (num1 > 0.0)
						heroObject.AddSkillXp(DefaultSkills.Riding, MBRandom.RoundRandomized(num1 * xpAmountAdjusted));
				}
				else
				{
					float num = 0.5f; // vanilla = 0.2f
					if (speedBonusFromMovement > 0.0)
						num += 1.0f * speedBonusFromMovement;
					if (num > 0.0)
						heroObject.AddSkillXp(DefaultSkills.Athletics, MBRandom.RoundRandomized(num * xpAmountAdjusted));
				}
			}
			if (commander == null || commander == affectorCharacter.HeroObject || commander.PartyBelongedTo == null)
				return false;
			// vanilla = "SkillLevelingManager.OnTacticsUsed(commander.PartyBelongedTo, (float)MathF.Ceiling(0.02f * f));"
			return false;
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager), "OnSimulationCombatKill")]
	internal class PatchOnSimulationCombatKill
	{
		private static bool Prefix(
		CharacterObject affectorCharacter,
		CharacterObject affectedCharacter,
		PartyBase affectorParty,
		PartyBase commanderParty)
		{
			int xpReward = Campaign.Current.Models.PartyTrainingModel.GetXpReward(affectedCharacter);
			if (affectorCharacter.IsHero)
			{
				if (!affectorCharacter.IsPlayerCharacter)
				{
					xpReward *= 5;
				}
				ItemObject defaultWeapon = CharacterHelper.GetDefaultWeapon(affectorCharacter);
				Hero heroObject = affectorCharacter.HeroObject;
				if (defaultWeapon != null)
				{
					SkillObject skillForWeapon = Campaign.Current.Models.CombatXpModel.GetSkillForWeapon(defaultWeapon.GetWeaponWithUsageIndex(0));
					heroObject.AddSkillXp(skillForWeapon, xpReward);
				}
                else
                {
					heroObject.AddSkillXp(DefaultSkills.Athletics, xpReward);
				}
				if (affectorCharacter.IsMounted)
				{
					float f = xpReward * 0.75f;
					heroObject.AddSkillXp(DefaultSkills.Riding, MBRandom.RoundRandomized(f));
				}
				else
				{
					float f = xpReward * 0.75f;
					heroObject.AddSkillXp(DefaultSkills.Athletics, MBRandom.RoundRandomized(f));
				}
			}
			if (commanderParty == null || !commanderParty.IsMobile || (commanderParty.LeaderHero == null || commanderParty.LeaderHero == affectedCharacter.HeroObject))
				return false;
			SkillLevelingManager.OnTacticsUsed(commanderParty.MobileParty, MathF.Ceiling(0.04f * xpReward * (!affectorCharacter.IsPlayerCharacter ? 0.5f : 1f)));
			return false;
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager))]
	[HarmonyPatch("OnSieging")]
	public static class PatchOnSieging
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			foreach (CodeInstruction ci in codes)
			{
				if (ci.opcode == OpCodes.Ldc_R4)
				{
					ci.operand = 30f; // vanilla = 5f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager))]
	[HarmonyPatch("OnSiegeEngineDestroyed")]
	public static class PatchOnSiegeEngineDestroyed
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			foreach (CodeInstruction ci in codes)
			{
				if (ci.opcode == OpCodes.Ldc_I4_S)
				{
					ci.operand = 5; // vanilla = 10
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager), "OnSiegeEngineBuilt")]
	internal class PatchOnSiegeEngineBuilt
	{
		private static bool Prefix(MobileParty mobileParty, SiegeEngineType siegeEngine)
		{
			float skillXp = (float)(30.0 + 5.0 * siegeEngine.Difficulty);
			SkillLevelingManager.OnPartyEngineerSkillExercised(mobileParty, DefaultSkills.Engineering, skillXp);
			return false;
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager))]
	[HarmonyPatch("OnBribeGiven")]
	public static class PatchOnBribeGiven
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			foreach (CodeInstruction ci in codes)
			{
				if (ci.opcode == OpCodes.Ldc_R4)
				{
					ci.operand = 0.5f; // vanilla = 0.1f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager))]
	[HarmonyPatch("OnMainHeroReleasedFromCaptivity")]
	public static class PatchOnMainHeroReleasedFromCaptivity
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			foreach (CodeInstruction ci in codes)
			{
				if (ci.opcode == OpCodes.Ldc_R4)
				{
					ci.operand = 4.0f; // vanilla = 0.5f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager))]
	[HarmonyPatch("OnMainHeroDisguised")]
	public static class PatchOnMainHeroDisguised
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			int num = 0;
			var codes = new List<CodeInstruction>(instructions);
			for (var i = 0; i < codes.Count; i++)
			{
				if (codes[i].opcode == OpCodes.Ldc_R4 && num == 0)
				{
					codes[i].operand = 800f; // vanilla = 1f
					i++;
					num++;
				}
				if (codes[i].opcode == OpCodes.Ldc_R4 && num == 1)
				{
					codes[i].operand = 600f; // vanilla = 10f
					i++;
					num++;
				}
				if (codes[i].opcode == OpCodes.Ldc_R4 && num == 2)
				{
					codes[i].operand = 45f; // vanilla = 10f
					i++;
					num++;
				}
				if (codes[i].opcode == OpCodes.Ldc_R4 && num == 3)
				{
					codes[i].operand = 65f; // vanilla = 25f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager))]
	[HarmonyPatch("OnPrisonerSell")]
	public static class PatchOnPrisonerSell
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			foreach (CodeInstruction ci in codes)
			{
				if (ci.opcode == OpCodes.Ldc_R4)
				{
					ci.operand = 20f; // vanilla = 6f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager))]
	[HarmonyPatch("OnRaiding")]
	public static class PatchOnRaiding
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			foreach (CodeInstruction ci in codes)
			{
				if (ci.opcode == OpCodes.Ldc_R4)
				{
					ci.operand = 25f; // vanilla = 4f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager))]
	[HarmonyPatch("OnAssaultingVillagersAndCaravans")]
	public static class PatchOnAssaultingVillagersAndCaravans
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			int num = 0;
			var codes = new List<CodeInstruction>(instructions);
			for (var i = 0; i < codes.Count; i++)
			{
				if (codes[i].opcode == OpCodes.Ldc_R4 && num == 0)
				{
					codes[i].operand = 30f; // vanilla = 3f
					i++;
					num++;
				}
				if (codes[i].opcode == OpCodes.Ldc_R4 && num == 1)
				{
					codes[i].operand = 10f; // vanilla = 1f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager))]
	[HarmonyPatch("OnHostileActionAgainstVillagersAndCaravans")]
	public static class PatchOnHostileActionAgainstVillagersAndCaravans
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			int num = 0;
			var codes = new List<CodeInstruction>(instructions);
			for (var i = 0; i < codes.Count; i++)
			{
				if (codes[i].opcode == OpCodes.Ldc_R4 && num == 0)
				{
					codes[i].operand = 30f; // vanilla = 1f
					i++;
					num++;
				}
				if (codes[i].opcode == OpCodes.Ldc_R4 && num == 1)
				{
					codes[i].operand = 10f; // vanilla = 0.5f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager))]
	[HarmonyPatch("OnForcingVillageGiveSupplies")]
	public static class PatchOnForcingVillageGiveSupplies
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			int num = 0;
			var codes = new List<CodeInstruction>(instructions);
			for (var i = 0; i < codes.Count; i++)
			{
				if (codes[i].opcode == OpCodes.Ldc_R4 && num == 0)
				{
					i++;
					num++;
				}
				if (codes[i].opcode == OpCodes.Ldc_R4 && num == 1)
				{
					codes[i].operand = 10f; // vanilla = 0.5f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager), "OnLeadingBandits")]
	internal class PatchOnLeadingBandits
	{
		private static void OnPersonalSkillExercised(Hero hero, SkillObject skill, float skillXp, bool shouldNotify = true)
		{
			hero?.HeroDeveloper.AddSkillXp(skill, skillXp, true, shouldNotify);
		}
		public static bool Prefix(MobileParty mobileParty, int banditCount)
		{
			OnPersonalSkillExercised(mobileParty.LeaderHero, DefaultSkills.Roguery, MathF.Round(0.15f * MathF.Pow(banditCount, 0.7f) * MathF.Pow((float)Math.Max(0, mobileParty.Morale - 60.0), 0.5f))); // vanilla = "SkillLevelingManager.OnLeaderSkillExercised(party.Party, DefaultSkills.Leadership, (float) MathF.Round((float) (0.01 * (double) party.MemberRoster.TotalManCount * ((double) party.Morale - 70.0))));"
			return false;
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager), "OnPrisonBreakEnd")]
	internal class PatchOnPrisonBreakEnd
	{
		public static bool Prefix(Hero prisonerHero, bool isSucceeded)
		{
			float rewardOnPrisonBreak = Campaign.Current.Models.PrisonBreakModel.GetRogueryRewardOnPrisonBreak(prisonerHero, isSucceeded);
			if (rewardOnPrisonBreak <= 0.0)
				return false;
			Hero.MainHero.AddSkillXp(DefaultSkills.Roguery, 0.5f * rewardOnPrisonBreak);
			return false;
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager))]
	[HarmonyPatch("OnTradeProfitMade")]
	[HarmonyPatch(new[] { typeof(PartyBase), typeof(int) })]
	public static class PatchOnTradeProfitMade1
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			foreach (CodeInstruction ci in codes)
			{
				if (ci.opcode == OpCodes.Ldc_R4)
				{
					ci.operand = 0.25f; // vanilla = 0.5f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager))]
	[HarmonyPatch("OnTradeProfitMade")]
	[HarmonyPatch(new[] { typeof(Hero), typeof(int) })]
	public static class PatchOnTradeProfitMade2
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			foreach (CodeInstruction ci in codes)
			{
				if (ci.opcode == OpCodes.Ldc_R4)
				{
					ci.operand = 0.25f; // vanilla = 0.5f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(DefaultDiplomacyModel))]
	[HarmonyPatch("GetCharmExperienceFromRelationGain")]
	public static class PatchGetCharmExperienceFromRelationGain
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			int num = 0;
			for (var i = 0; i < codes.Count; i++)
			{
				if (num == 0 && codes[i].opcode == OpCodes.Ldc_R4)
				{
					codes[i].operand = 40f; // vanilla = 20f
					num++;
					i++;
				}
				if (num == 1 && codes[i].opcode == OpCodes.Ldc_R4)
				{
					codes[i].operand = 5f; // vanilla = 30f
					num++;
					i++;
				}
				if (num == 2 && codes[i].opcode == OpCodes.Ldc_R4)
				{
					codes[i].operand = 3f; // vanilla = 20f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager))]
	[HarmonyPatch("OnSettlementProjectFinished")]
	public static class PatchOnSettlementProjectFinished
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			foreach (CodeInstruction ci in codes)
			{
				if (ci.opcode == OpCodes.Ldc_R4)
				{
					ci.operand = 5000f; // vanilla = 1000f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager))]
	[HarmonyPatch("OnSettlementGoverned")]
	public static class PatchOnSettlementGoverned
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			foreach (CodeInstruction ci in codes)
			{
				if (ci.opcode == OpCodes.Ldc_R4)
				{
					ci.operand = 60f; // vanilla = 30f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(SkillLevelingManager))]
	[HarmonyPatch("OnInfluenceSpent")]
	public static class PatchOnInfluenceSpent
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			foreach (CodeInstruction ci in codes)
			{
				if (ci.opcode == OpCodes.Ldc_R4)
				{
					ci.operand = 20f; // vanilla = 10f
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}

	[HarmonyPatch(typeof(CraftingCampaignBehavior), "GetTownOrderDifficulty")]
	internal class PatchGetTownOrderDifficulty
	{
		public static bool Prefix(Town town, int orderSlot, ref float __result)
		{
			int num1 = MBRandom.RandomInt(0, 6);
			int num2 = 0;
			switch (num1)
			{
				case 0:
					num2 = MBRandom.RandomInt(40, 80);
					break;
				case 1:
					num2 = MBRandom.RandomInt(80, 120);
					break;
				case 2:
					num2 = MBRandom.RandomInt(120, 160);
					break;
				case 3:
					num2 = MBRandom.RandomInt(160, 200);
					break;
				case 4:
					num2 = MBRandom.RandomInt(200, 241);
					break;
				case 5:
					num2 = Hero.MainHero.GetSkillValue(DefaultSkills.Crafting);
					break;
			}
			__result = Math.Min(299, num2 + town.Prosperity / 500f);
			return false;
		}
	}

	// ------------ Plus Version Tweaks ----------------
	/*
	[HarmonyPatch(typeof(DefaultItemValueModel), "GetEquipmentValueFromTier")]
	internal class PatchGetEquipmentValueFromTier
	{
		private static bool Prefix(ref float __result, float itemTierf)
		{
			itemTierf = MathF.Clamp(itemTierf, -1f, 7.5f);
			if (itemTierf > 1)
            {
				itemTierf = MathF.Pow(itemTierf, 0.75f);
			}
			__result = (float)Math.Pow(3.0, itemTierf); // vanilla = "(float)Math.Pow(3.0, MathF.Clamp(itemTierf, -1f, 7.5f));"
			return false;
		}
	}

	[HarmonyPatch(typeof(DefaultItemValueModel), "CalculateTierMeleeWeapon")]
	internal class PatchCalculateTierMeleeWeapon
	{
		private static float GetFactor(DamageTypes swingDamageType, WeaponClass WeaponClass)
		{
			if (swingDamageType == DamageTypes.Blunt && (WeaponClass == WeaponClass.TwoHandedPolearm || WeaponClass == WeaponClass.OneHandedPolearm))
				return 1.3f;
			else if (swingDamageType == DamageTypes.Blunt)
				return 1.55f;
			else if (swingDamageType != DamageTypes.Pierce && (WeaponClass == WeaponClass.TwoHandedPolearm || WeaponClass == WeaponClass.OneHandedPolearm))
				return 0.75f;
			else
				return swingDamageType != DamageTypes.Pierce ? 1f : 1.9f;
		}
		private static bool Prefix(ref float __result, WeaponComponent weaponComponent)
		{
			WeaponComponentData weapon = weaponComponent.Weapons[0];
			ItemObject itemObject = weaponComponent.Item;
			if (itemObject != null)
			{
				int itemType = (int)itemObject.ItemType;
			}
			int weaponClass = (int)weapon.WeaponClass;
			float num1 = Math.Max((float)weapon.ThrustDamage * PatchCalculateTierMeleeWeapon.GetFactor(weapon.ThrustDamageType, weapon.WeaponClass) * MathF.Pow((float)weapon.ThrustSpeed * 0.01f, 1.5f), (float)weapon.SwingDamage * PatchCalculateTierMeleeWeapon.GetFactor(weapon.SwingDamageType, weapon.WeaponClass) * MathF.Pow((float)weapon.SwingSpeed * 0.01f, 1.5f) * 1.1f);
			if (weapon.WeaponFlags.HasAnyFlag<WeaponFlags>(WeaponFlags.NotUsableWithOneHand))
				num1 *= 0.7f;
			if (weapon.WeaponClass == WeaponClass.OneHandedAxe)
				num1 *= 1.2f;
			if (weapon.WeaponClass == WeaponClass.TwoHandedAxe)
				num1 *= 1.15f;
			if (weapon.WeaponClass == WeaponClass.TwoHandedPolearm)
				num1 *= 1.2f;
			if (weapon.WeaponClass == WeaponClass.ThrowingKnife)
				num1 *= 1.4f;
			if (weapon.WeaponClass == WeaponClass.Javelin)
			{
				num1 *= 0.28f;
				num1 += 20f;
			}
			if (weapon.WeaponClass == WeaponClass.ThrowingAxe)
				num1 *= 1.0f;
			float num2 = (float)weapon.WeaponLength * 0.01f;
			__result = (float)(0.06 * ((double)num1 * (1.0 + (double)num2)) - 3.5);
			return false;
		}
	}

	[HarmonyPatch(typeof(DefaultSmithingModel), "ResearchPointsNeedForNewPart")]
	internal class PatchResearchPointsNeedForNewPart
	{
		private static void Postfix(ref int __result)
		{
			__result = 200; // vanilla = "(count * count + 12) / 4;"
		}
	}

	[HarmonyPatch(typeof(DefaultSmithingModel), "GetPartResearchGainForSmeltingItem")]
	internal class PatchGetPartResearchGainForSmeltingItem
	{
		private static bool Prefix(ref int __result, ItemObject item, Hero hero)
		{
			int num = 40 + MathF.Round((float)(4 * Math.Pow((float)item.Value, 0.42f))); // vanilla = "int num = 1 + MathF.Floor(0.02f * (float) item.Value);"
			if (hero.GetPerkValue(DefaultPerks.Crafting.CuriousSmelter))
				num *= 2;
			__result = num;
			return false;
		}
	}

	[HarmonyPatch(typeof(DefaultSmithingModel), "GetPartResearchGainForSmithingItem")]
	internal class PatchGetPartResearchGainForSmithingItem
	{
		private static bool Prefix(ref int __result, ItemObject item, Hero hero)
		{
			int num = 200 + MathF.Round((float)(20 * Math.Pow((float)item.Value, 0.42f))); // vanilla = "int num = 1 + MathF.Floor(0.1f * (float) item.Value);"
			if (hero.GetPerkValue(DefaultPerks.Crafting.CuriousSmith))
				num *= 2;
			__result = num;
			return false;
		}
	}

	[HarmonyPatch(typeof(SandboxAgentStatCalculateModel))]
	[HarmonyPatch("UpdateHumanStats")]
	public static class PatchUpdateHumanStats
	{
		static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			var codes = new List<CodeInstruction>(instructions);
			int num = 0;
			for (var i = 0; i < codes.Count; i++)
			{
				if (num == 0 && codes[i].opcode == OpCodes.Ldc_R4 && (float)codes[i].operand == 0.7f)
				{
					i++;
					if (num == 0 && codes[i].opcode == OpCodes.Ldc_R4 && (float)codes[i].operand == 1f)
					{
						i++;
						if (num == 0 && codes[i].opcode == OpCodes.Ldc_R4 && (float)codes[i].operand == 3f)
						{
							codes[i].operand = 5f; // vanilla = 3f
							i++;
							num++;
						}
					}
				}
				if (num == 1 && codes[i].opcode == OpCodes.Ldc_R4 && (float)codes[i].operand == 0.2f)
				{
					codes[i].operand = 0.35f; // base speed is 0.7 so this is 50% of that
					i++;
					for (int j = 0; j < 10; j++)
                    {
						codes[i].opcode = OpCodes.Nop;
						i++;
					}
					i++;
					codes[i].opcode = OpCodes.Ldc_R4;
					codes[i].operand = 60f; // this is the weight at which you have a -50% penalty
					i++;
					codes[i].opcode = OpCodes.Div;
					i++;
					codes[i].opcode = OpCodes.Mul;
					i++;
					codes[i].opcode = OpCodes.Sub;
					i++;
					for (int k = 0; k < 6; k++)
					{
						codes[i].opcode = OpCodes.Nop;
						i++;
					}
					break;
				}
			}
			return codes.AsEnumerable();
		}
	}
	*/

}
