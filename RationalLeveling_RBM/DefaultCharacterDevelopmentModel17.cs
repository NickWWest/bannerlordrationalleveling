﻿// here to track what we've changed vs what was vanilla.
// // Decompiled with JetBrains decompiler
// // Type: TaleWorlds.CampaignSystem.SandBox.GameComponents.DefaultCharacterDevelopmentModel
// // Assembly: TaleWorlds.CampaignSystem, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// // MVID: 84D052B3-0163-4B71-8B62-917BA372E85D
// // Assembly location: C:\Program Files (x86)\Steam\steamapps\common\Mount & Blade II Bannerlord\bin\Win64_Shipping_Client\TaleWorlds.CampaignSystem.dll
//
// namespace TaleWorlds.CampaignSystem.SandBox.GameComponents
// {
//   public class DefaultCharacterDevelopmentModel : CharacterDevelopmentModel
//   {
//     private const int MaxCharacterLevels = 62;
//     private const int MaxAttributeLevel = 11;
//     private const int SkillPointsAtLevel1 = 1;
//     private const int SkillPointsGainNeededInitialValue = 1000;
//     private const int SkillPointsGainNeededIncreasePerLevel = 1000;
//     private readonly int[] _skillsRequiredForLevel = new int[63];
//     private const int FocusPointsPerLevelConst = 1;
//     private const int LevelsPerAttributePointConst = 4;
//     private const int FocusPointCostToOpenSkillConst = 0;
//     private const int FocusPointsAtStartConst = 5;
//     private const int AttributePointsAtStartConst = 15;
//     private const int MaxSkillLevels = 1024;
//     private readonly int[] _xpRequiredForSkillLevel = new int[1024];
//     private const int XpRequirementForFirstLevel = 30;
//     private const int MaxSkillPoint = 2147483647;
//     private const float BaseLearningRate = 1.25f;
//     private const int traitThreshold1 = 1000;
//     private const int traitThreshold2 = 4000;
//     private const int traitMaxValue1 = 2500;
//     private const int traitMaxValue2 = 6000;
//     private static TextObject _attributeText = new TextObject("{=AT6v10NK}Attribute");
//     private static TextObject _skillFocusText = new TextObject("{=MRktqZwu}Skill Focus");
//     private static TextObject _overLimitText = new TextObject("{=bcA7ZuyO}Learning Limit Exceeded");
//
//     public DefaultCharacterDevelopmentModel()
//     {
//       this.InitializeSkillsRequiredForLevel();
//       this.InitializeXpRequiredForSkillLevel();
//     }
//
//     private void InitializeSkillsRequiredForLevel()
//     {
//       int num1 = 1000;
//       int num2 = 1;
//       this._skillsRequiredForLevel[0] = 0;
//       this._skillsRequiredForLevel[1] = 1;
//       for (int index = 2; index < this._skillsRequiredForLevel.Length; ++index)
//       {
//         num2 += num1;
//         this._skillsRequiredForLevel[index] = num2;
//         num1 += 1000 + num1 / 5;
//       }
//     }
//
//     public override int SkillsRequiredForLevel(int level) => level > 62 ? int.MaxValue : this._skillsRequiredForLevel[level];
//
//     public override int GetMaxSkillPoint() => int.MaxValue;
//
//     private void InitializeXpRequiredForSkillLevel()
//     {
//       int num = 30;
//       this._xpRequiredForSkillLevel[0] = num;
//       for (int index = 1; index < 1024; ++index)
//       {
//         num += 10 + index;
//         this._xpRequiredForSkillLevel[index] = this._xpRequiredForSkillLevel[index - 1] + num;
//       }
//     }
//
//     public override int GetXpRequiredForSkillLevel(int skillLevel) => skillLevel <= 0 ? 0 : this._xpRequiredForSkillLevel[skillLevel - 1];
//
//     public override void GetSkillLevelChange(
//       Hero hero,
//       SkillObject skill,
//       float skillXp,
//       out int skillLevelChange)
//     {
//       skillLevelChange = 0;
//       int skillValue = hero.GetSkillValue(skill);
//       for (int index1 = 0; index1 < 1024; ++index1)
//       {
//         int index2 = skillValue + index1;
//         if (index2 < 1023)
//         {
//           if ((double) skillXp < (double) this._xpRequiredForSkillLevel[index2])
//             break;
//           ++skillLevelChange;
//         }
//       }
//     }
//
//     public override int GetXpAmountForSkillLevelChange(
//       Hero hero,
//       SkillObject skill,
//       int skillLevelChange)
//     {
//       int skillValue = hero.GetSkillValue(skill);
//       return this._xpRequiredForSkillLevel[skillValue + skillLevelChange] - this._xpRequiredForSkillLevel[skillValue];
//     }
//
//     public override void GetTraitLevelForTraitXp(
//       Hero hero,
//       TraitObject trait,
//       int xpValue,
//       out int traitLevel,
//       out int clampedTraitXp)
//     {
//       clampedTraitXp = xpValue;
//       int num1 = trait.MinValue < -1 ? -6000 : (trait.MinValue == -1 ? -2500 : 0);
//       int num2 = trait.MaxValue > 1 ? 6000 : (trait.MaxValue == 1 ? 2500 : 0);
//       if (xpValue > num2)
//         clampedTraitXp = num2;
//       else if (xpValue < num1)
//         clampedTraitXp = num1;
//       traitLevel = clampedTraitXp <= -4000 ? -2 : (clampedTraitXp <= -1000 ? -1 : (clampedTraitXp < 1000 ? 0 : (clampedTraitXp < 4000 ? 1 : 2)));
//       if (traitLevel < trait.MinValue)
//       {
//         traitLevel = trait.MinValue;
//       }
//       else
//       {
//         if (traitLevel <= trait.MaxValue)
//           return;
//         traitLevel = trait.MaxValue;
//       }
//     }
//
//     public override int GetTraitXpRequiredForTraitLevel(TraitObject trait, int traitLevel)
//     {
//       if (traitLevel < -1)
//         return -4000;
//       if (traitLevel == -1)
//         return -1000;
//       if (traitLevel == 0)
//         return 0;
//       return traitLevel != 1 ? 4000 : 1000;
//     }
//
//     public override int AttributePointsAtStart => 15;
//
//     public override int LevelsPerAttributePoint => 4;
//
//     public override int FocusPointsPerLevel => 1;
//
//     public override int FocusPointsAtStart => 5;
//
//     public override int FocusPointCostToOpenSkill => 0;
//
//     public override void GetInitialSkillXpForCharacter(
//       Hero hero,
//       SkillObject skill,
//       out int initialSkillXp)
//     {
//       int skillValue = hero.CharacterObject.GetSkillValue(skill);
//       if (skillValue >= 1024)
//         initialSkillXp = this._xpRequiredForSkillLevel[1023];
//       else if (skillValue <= 0)
//         initialSkillXp = 0;
//       else
//         initialSkillXp = this._xpRequiredForSkillLevel[skillValue];
//     }
//
//     public override int GetDevelopmentPointNeededToChangeTrait(float traitValue) => MathF.Round(MathF.Abs(traitValue));
//
//     public override ExplainedNumber CalculateLearningLimit(
//       int attributeValue,
//       int focusValue,
//       TextObject attributeName,
//       bool includeDescriptions = false)
//     {
//       ExplainedNumber learningLimit = new ExplainedNumber(includeDescriptions: includeDescriptions);
//       learningLimit.Add((float) ((attributeValue - 1) * 10), attributeName);
//       learningLimit.Add((float) (focusValue * 30), DefaultCharacterDevelopmentModel._skillFocusText);
//       learningLimit.LimitMin(0.0f);
//       return learningLimit;
//     }
//
//     public override float CalculateLearningRate(Hero hero, SkillObject skill)
//     {
//       int level = hero.Level;
//       return this.CalculateLearningRate(hero.GetAttributeValue(skill.CharacterAttribute), hero.HeroDeveloper.GetFocus(skill), hero.GetSkillValue(skill), level, skill.CharacterAttribute.Name, false).ResultNumber;
//     }
//
//     public override ExplainedNumber CalculateLearningRate(
//       int attributeValue,
//       int focusValue,
//       int skillValue,
//       int characterLevel,
//       TextObject attributeName,
//       bool includeDescriptions = false)
//     {
//       ExplainedNumber learningRate = new ExplainedNumber(1.25f, includeDescriptions);
//       learningRate.AddFactor(0.4f * (float) attributeValue, attributeName);
//       learningRate.AddFactor((float) focusValue * 1f, DefaultCharacterDevelopmentModel._skillFocusText);
//       int num1 = MathF.Round(this.CalculateLearningLimit(attributeValue, focusValue, (TextObject) null, false).ResultNumber);
//       if (skillValue > num1)
//       {
//         int num2 = skillValue - num1;
//         learningRate.AddFactor((float) (-1.0 - 0.100000001490116 * (double) num2), DefaultCharacterDevelopmentModel._overLimitText);
//       }
//       learningRate.LimitMin(0.0f);
//       return learningRate;
//     }
//   }
// }